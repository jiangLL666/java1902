package com.jiangLL.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiangLL.dao.AttributeDAO;
import com.jiangLL.dao.ProductDAO;
import com.jiangLL.dao.UserDAO;
import com.jiangLL.entity.PageQuery;
import com.jiangLL.entity.Product;
import com.jiangLL.entity.ProductAttribute;
import com.jiangLL.entity.ProductAttributeType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:/applicationContext-dao.xml"})
public class DAOTest {

	@Autowired
	private UserDAO userDAO;
	@Autowired
	private AttributeDAO attributeDAO;
	
	@Autowired
	private ProductDAO productDAO;
	
	@Test
	public void selectTotal() {
		Integer total = productDAO.selectTotal();
		System.out.println(total);
	}
	
	@Test
	public void selectPageById() {
		List<Product> list = productDAO.selectPageById(new PageQuery(0,3));
		System.out.println(list);
	}
	@Test
	public void attributeDAO() {
		List<ProductAttribute> selectAttributesByNo = attributeDAO.selectAttributesByNo(2);
		System.out.println(selectAttributesByNo);
		List<ProductAttributeType> totalAttributeTypes = attributeDAO.totalAttributeTypes();
		System.out.println(totalAttributeTypes);
	}
	
}
