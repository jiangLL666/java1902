package com.jiangLL.service;

import java.util.List;

import com.jiangLL.entity.ProductAttribute;
import com.jiangLL.entity.ProductAttributeType;

public interface ProductAttributeService {

	public List<ProductAttributeType> totalAttributeTypes();
	
	public List<ProductAttribute> selectAttributesByNo(Integer attributeTypeId);
}
