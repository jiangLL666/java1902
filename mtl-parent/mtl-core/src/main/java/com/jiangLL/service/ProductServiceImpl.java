package com.jiangLL.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jiangLL.dao.ProductDAO;
import com.jiangLL.entity.PageBean;
import com.jiangLL.entity.PageQuery;
import com.jiangLL.entity.Product;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO;
	
	@Override
	public PageBean<Product> selectProductListByPage(Integer currentPage) {
		PageBean<Product> pageBean = new PageBean<Product>();
		pageBean.setCurrentPage(currentPage);
		int pageSize = 2;
		pageBean.setPageSize(pageSize);
		Integer totalSize = selectTotalSize();
		pageBean.setTotalSize(totalSize);
		Integer totalPage = (totalSize % pageSize ==0)? totalSize / pageSize : totalSize / pageSize +1;
		pageBean.setTotalPage(totalPage);
		int begin = (currentPage-1)*pageSize;
		List<Product> list = productDAO.selectPageById(new PageQuery(begin,pageSize));
		pageBean.setList(list);
		return pageBean;
	}

	@Override
	public Integer selectTotalSize() {
		return productDAO.selectTotal();
	}

}
