package com.jiangLL.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jiangLL.dao.AttributeDAO;
import com.jiangLL.entity.ProductAttribute;
import com.jiangLL.entity.ProductAttributeType;

@Service
public class ProductAttributeServiceImpl implements ProductAttributeService {

	@Autowired
	private AttributeDAO attributeDAO;
	
	@Override
	public List<ProductAttributeType> totalAttributeTypes() {
		return attributeDAO.totalAttributeTypes();
	}

	@Override
	public List<ProductAttribute> selectAttributesByNo(Integer attributeTypeId) {
		return attributeDAO.selectAttributesByNo(attributeTypeId);
	}

}
