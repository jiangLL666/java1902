package com.jiangLL.service;

import com.jiangLL.entity.PageBean;
import com.jiangLL.entity.Product;

public interface ProductService {

	public PageBean<Product> selectProductListByPage(Integer currentPage);
	
	public Integer selectTotalSize();
	
}
