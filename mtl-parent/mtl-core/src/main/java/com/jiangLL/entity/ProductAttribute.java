package com.jiangLL.entity;

public class ProductAttribute {

	private Integer attrId;
	private String attrName;
	private String attrDes;
	private Integer attrTypeNo;
	
	public ProductAttribute() {
		// TODO Auto-generated constructor stub
	}

	public ProductAttribute(Integer attrId, String attrName, String attrDes, Integer attrTypeNo) {
		super();
		this.attrId = attrId;
		this.attrName = attrName;
		this.attrDes = attrDes;
		this.attrTypeNo = attrTypeNo;
	}

	@Override
	public String toString() {
		return "ProductAttribute [attrId=" + attrId + ", attrName=" + attrName + ", attrDes=" + attrDes
				+ ", attrTypeNo=" + attrTypeNo + "]";
	}

	public Integer getAttrId() {
		return attrId;
	}

	public void setAttrId(Integer attrId) {
		this.attrId = attrId;
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getAttrDes() {
		return attrDes;
	}

	public void setAttrDes(String attrDes) {
		this.attrDes = attrDes;
	}

	public Integer getAttrTypeNo() {
		return attrTypeNo;
	}

	public void setAttrTypeNo(Integer attrTypeNo) {
		this.attrTypeNo = attrTypeNo;
	} 	
}
