package com.jiangLL.entity;

import java.util.List;

public class ProductAttributeType {

	private Integer attrTypeId;
	private String attrTypeName;
	private Integer bak1;
	private String attrTypeNameEn;
	private List<ProductAttribute> productAttributeList;
	
	public ProductAttributeType() {
		super();
	}

	public ProductAttributeType(Integer attrTypeId, String attrTypeName, Integer bak1, String attrTypeNameEn,
			List<ProductAttribute> productAttributeList) {
		super();
		this.attrTypeId = attrTypeId;
		this.attrTypeName = attrTypeName;
		this.bak1 = bak1;
		this.attrTypeNameEn = attrTypeNameEn;
		this.productAttributeList = productAttributeList;
	}

	@Override
	public String toString() {
		return "ProductAttributeType [attrTypeId=" + attrTypeId + ", attrTypeName=" + attrTypeName + ", bak1=" + bak1
				+ ", attrTypeNameEn=" + attrTypeNameEn + ", productAttributeList=" + productAttributeList + "]";
	}

	public Integer getAttrTypeId() {
		return attrTypeId;
	}

	public void setAttrTypeId(Integer attrTypeId) {
		this.attrTypeId = attrTypeId;
	}

	public String getAttrTypeName() {
		return attrTypeName;
	}

	public void setAttrTypeName(String attrTypeName) {
		this.attrTypeName = attrTypeName;
	}

	public Integer getBak1() {
		return bak1;
	}

	public void setBak1(Integer bak1) {
		this.bak1 = bak1;
	}

	public String getAttrTypeNameEn() {
		return attrTypeNameEn;
	}

	public void setAttrTypeNameEn(String attrTypeNameEn) {
		this.attrTypeNameEn = attrTypeNameEn;
	}

	public List<ProductAttribute> getProductAttributeList() {
		return productAttributeList;
	}

	public void setProductAttributeList(List<ProductAttribute> productAttributeList) {
		this.productAttributeList = productAttributeList;
	}	
}
