package com.jiangLL.entity;

public class Product {

	private Integer productId;
	private String productCode;
	private String productName;
	private Integer productType;
	private Integer productBrand;
	private String imageId;
	private Integer productMinPrice;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}

	public Product(Integer productId, String productCode, String productName, Integer productType, Integer productBrand,
			String imageId, Integer productMinPrice) {
		super();
		this.productId = productId;
		this.productCode = productCode;
		this.productName = productName;
		this.productType = productType;
		this.productBrand = productBrand;
		this.imageId = imageId;
		this.productMinPrice = productMinPrice;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productCode=" + productCode + ", productName=" + productName
				+ ", productType=" + productType + ", productBrand=" + productBrand + ", imageId=" + imageId
				+ ", productMinPrice=" + productMinPrice + "]";
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(Integer productBrand) {
		this.productBrand = productBrand;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public Integer getProductMinPrice() {
		return productMinPrice;
	}

	public void setProductMinPrice(Integer productMinPrice) {
		this.productMinPrice = productMinPrice;
	}
	
}
