package com.jiangLL.dao;

import java.util.List;

import com.jiangLL.entity.ProductAttribute;
import com.jiangLL.entity.ProductAttributeType;

public interface AttributeDAO {

	public List<ProductAttributeType> totalAttributeTypes();
	
	public List<ProductAttribute> selectAttributesByNo(Integer attributeTypeId);
}
