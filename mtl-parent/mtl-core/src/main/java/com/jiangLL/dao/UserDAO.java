package com.jiangLL.dao;

import java.util.List;

import com.jiangLL.entity.User;

public interface UserDAO {

	public List<User> selectUserList();
	
}
