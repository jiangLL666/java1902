package com.jiangLL.dao;

import java.util.List;

import com.jiangLL.entity.PageBean;
import com.jiangLL.entity.PageQuery;
import com.jiangLL.entity.Product;

public interface ProductDAO {

	public List<Product> selectPageById(PageQuery pageQuery);
	
	public Integer selectTotal();
	
}
