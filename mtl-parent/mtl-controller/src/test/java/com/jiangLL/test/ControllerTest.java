package com.jiangLL.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jiangLL.entity.PageBean;
import com.jiangLL.entity.Product;
import com.jiangLL.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath*:/applicationContext-dao.xml", "classpath*:/applicationContext-service.xml"})
public class ControllerTest {

	@Autowired
	private ProductService productService;
	

	@Test
	public void test01() {
		PageBean<Product> pageBean = productService.selectProductListByPage(1);
		System.out.println(pageBean);
	}
	
}
