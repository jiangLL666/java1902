package com.jiangLL.controller;

import com.jiangLL.entity.ProductAttributeType;
import com.jiangLL.service.ProductAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/info")
public class InfoController {
	
	@Autowired
	private ProductAttributeService productAttributeService;
	
	@RequestMapping("/toInfo.do")
	public String toIndextoInfo(Model model) {		
		List<ProductAttributeType> attributes = productAttributeService.totalAttributeTypes();
		model.addAttribute("attributes", attributes);
		return "info";
	}
	
	@RequestMapping("/getPrice.do")
	public String getPrice(String desc_ids) {
		System.out.println(desc_ids);
		return "price";
	}
}
