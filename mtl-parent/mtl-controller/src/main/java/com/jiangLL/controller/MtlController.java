package com.jiangLL.controller;

import java.lang.ProcessBuilder.Redirect;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jiangLL.entity.PageBean;
import com.jiangLL.entity.Product;
import com.jiangLL.service.ProductService;

@Controller
public class MtlController {

	@Autowired
	private ProductService productService;
	
	@RequestMapping("/toLogin.do")
	public String toLogin(String username,String password) {
		if ("root".equals(username)  && "root".equals(password) ) {
			return "redirect:toIndex.do";
		}else {
			return "fails";			
		}		
	}
	
	@RequestMapping("/toIndex.do")
	public String toIndex(Model model ,Integer currentPage) {
		if (null == currentPage) {
			currentPage = 1;
		}
		PageBean<Product> pageBean = productService.selectProductListByPage(currentPage);
		model.addAttribute("pageBean", pageBean);
		return "index";
	}
	
	@RequestMapping("/selectProductListByPage.do")
	public String selectProductListByPage(Model model,Integer currentPage) {
		if (null == currentPage) {
			currentPage = 1;
		}
		PageBean<Product> pageBean = productService.selectProductListByPage(currentPage);
		model.addAttribute("pageBean", pageBean);
		return "index";
	}
	
}
